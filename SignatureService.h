#import <CommonCrypto/CommonCrypto.h>
#import <Foundation/Foundation.h>

@interface SignatureService: NSObject

+(NSURL*) generateURLWithDevIDAndKey;

@end