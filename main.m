#import <Foundation/Foundation.h>
#import "SignatureService.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
		NSURL *url = [SignatureService generateURLWithDevIDAndKey];
		NSString *urlString = [url absoluteString];

		NSLog(@"URL: %@", urlString);
	}

	return 0;
}
