#import "SignatureService.h"

@interface SignatureService()
@end

@implementation SignatureService

+(NSURL*) generateURLWithDevIDAndKey {

	NSString *hardcodedURL = @"https://timetableapi.ptv.vic.gov.au";
	NSString *hardcodedDevID = @"";
	NSString *hardcodedkey = @"";

	NSString *urlPath = @"https://timetableapi.ptv.vic.gov.au/v3/routes";

	NSRange deleteRange = { 0, [hardcodedURL length] };
	NSMutableString *urlString = [[NSMutableString alloc]initWithString:urlPath];

	// Remove the base path from the urlString
	[urlString deleteCharactersInRange:deleteRange];

	if ([urlString containsString:@"?"]) {
		[urlString appendString:@"&"];
	} else {
		[urlString appendString:@"?"];
	}

	[urlString appendFormat:@"devid=%@", hardcodedDevID];

	// Convert the key and suffix of URL into a character array
	const char *cKey = [hardcodedkey cStringUsingEncoding:NSUTF8StringEncoding];
	const char *cData = [urlString cStringUsingEncoding:NSUTF8StringEncoding];
	unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];

	// Sign the suffix of the URL with a SHA-HMAC1 algorithm
	CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);

	NSString *hash;
	NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];

	// Convert the char characters into hexadecimal digits
	for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
		[output appendFormat:@"%02x", cHMAC[i]];
	}

	hash = output;

	NSString* signature = [hash uppercaseString];
	NSString *urlSuffix = [NSString stringWithFormat:@"devid=%@&signature=%@", hardcodedDevID,signature];
	NSURL *url = [NSURL URLWithString:urlPath];
	NSString *urlQuery = [url query];

	// Append developer ID and signature to path
	if(urlQuery != nil && [urlQuery length] > 0) {
		url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&%@", urlPath, urlSuffix]];
	} else {
		url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", urlPath, urlSuffix]];
	}

	return url;
}

@end