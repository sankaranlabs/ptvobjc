# Signature generator for PTV API

This project sets up the sample Objective C code provided in the PTV API documentation into
something that outputs a live URL and is a functioning example of signature calculation.

## Background

PTV requires all requests to be signed with HMAC-SHA1. The documentation provides examples in
a few languages of how to do this, but it was quite difficult to read and understand what was
happening. To that end, I have gone ahead and set up this project with comments to break down
what the steps are, and to generate a sample URL that you can use to hit the PTV API.

## Get started

Enter your deveveloper ID and security key provided by PTV into `SignatureService.m`,
and compile the code using `clang -framework Foundation main.m SignatureService.m -o main`.